package ru.edu.lesson1;

import java.math.RoundingMode;

public class MyDigitConvertHard implements DigitConverterHard {

    @Override
    public String convert(double digit, int radix, int precision) {
        double tmp_digit = digit;
        int tmp_precision = precision;

        if (tmp_digit < 0 | radix < 1) {
            throw new IllegalArgumentException("digit " + digit +
                    " out of range!");
        }

        int tmp = (int) digit;
        double tmp_after = (digit * Math.pow(10.0D, (double) precision) - (double) tmp * Math.pow(10.0D, (double) precision)) / Math.pow(10.0D, (double) precision);
        String result = "";
        if (tmp == 0) {
            result = "0";
        }

        while (tmp > 0) {
            result = tmp % radix + result;
            tmp /= radix;
        }

        for (result = result + "."; tmp_precision > 0; --tmp_precision) {
            tmp_after = tmp_after * Math.pow(10.0D, (double) precision) * (double) radix / Math.pow(10.0D, (double) precision);
            result = result + (int) tmp_after;
            tmp_after = (double) ((int) (tmp_after * Math.pow(10.0D, (double) precision) - (double) ((int) tmp_after) * Math.pow(10.0D, (double) precision) + 0.5D)) / Math.pow(10.0D, (double) precision);
        }

        return result;
    }

    @Override
    public String convert(int digit, int radix) {
        return "";
    }
}
