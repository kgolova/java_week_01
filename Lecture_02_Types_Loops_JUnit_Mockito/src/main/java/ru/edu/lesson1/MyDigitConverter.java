package ru.edu.lesson1;

public class MyDigitConverter implements DigitConverter {
    @Override
    public String convert(int digit, int radix) {

        int tmp = digit;
        String result = "";

        if (tmp < 0 | radix < 1) {
            throw new IllegalArgumentException("digit " + digit +
                    " out of range!");
        }


        if (digit == 0) {
            return "0";
        }
        while (tmp > 0) {
            result = tmp % radix + result;
            tmp /= radix;
        }

        return result;

    }

}
