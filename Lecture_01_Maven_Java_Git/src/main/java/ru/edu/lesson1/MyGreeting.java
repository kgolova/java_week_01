package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.List;

public class MyGreeting implements IGreeting {

    private String firstName = "Kseniya";
    private String secondName = "Mihailovna";
    private String lastName = "Golova";
    private String bitbucketUrl = "https://KGolova@bitbucket.org";
    private String phone = "89201112233";
    private String courseExpectation = "Java";
    private List<Hobby> hobbies = new ArrayList<>();

    public MyGreeting() {
        hobbies.add(new Hobby("1", "Hobby 1", "Hobby 1"));
        hobbies.add(new Hobby("2", "Hobby 2", "Hobby 2"));
        hobbies.add(new Hobby("3", "Hobby 3", "Hobby 3"));
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getSecondName() {
        return secondName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public List<Hobby> getHobbies() {
        return hobbies;
    }

    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getCourseExpectation() {
        return courseExpectation;
    }


    @Override
    public String toString() {
        return "MyGreeting{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", bitbucketUrl=" + bitbucketUrl +
                '}';
    }

    @Override
    public String getEducationInfo() {
        return toString();
    }
}
