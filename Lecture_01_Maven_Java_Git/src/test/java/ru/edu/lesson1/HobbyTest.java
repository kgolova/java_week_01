package ru.edu.lesson1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class HobbyTest {
    private Hobby hobby1;
    private Hobby hobby2;

    @Before
    public void setUp() {
        hobby1 = new Hobby("1", "Hobby 1", "Hobby 1");
        hobby2 = new Hobby("2", "Hobby 2");
    }

    @Test
    public void size_Test() {

        Assert.assertNotNull(hobby1);

        assertEquals("1", hobby1.getId());
        assertEquals("Hobby 1", hobby1.getName());
        assertEquals("Hobby 1", hobby1.getDescription());
        assertEquals("Hobby{id='1', name='Hobby 1', description='Hobby 1'}", hobby1.toString());

    }

    @Test
    public void getId() {
        Assert.assertNotNull(hobby2);
        assertEquals("2", hobby2.getId());
    }

    @Test
    public void getName() {
        Assert.assertNotNull(hobby2);
        assertEquals("Hobby 2", hobby2.getName());
    }

    @Test
    public void getDescription() {
        Assert.assertNotNull(hobby2);
        assertEquals("", hobby2.getDescription());
    }

}