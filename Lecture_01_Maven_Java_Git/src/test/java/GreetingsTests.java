import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.Hobby;
import ru.edu.lesson1.IGreeting;
import ru.edu.lesson1.MyGreeting;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;

public class GreetingsTests {

    private IGreeting greeting = new MyGreeting();
    private Collection<Hobby> hobbies = new ArrayList<>();


    @Test
    public void size_Test() {

        Assert.assertNotNull(greeting);

        hobbies = greeting.getHobbies();

        assertEquals("Kseniya", greeting.getFirstName());
        assertEquals("Mihailovna", greeting.getSecondName());
        assertEquals("Golova", greeting.getLastName());
        assertEquals("https://KGolova@bitbucket.org", greeting.getBitbucketUrl());
        assertEquals("89201112233", greeting.getPhone());
        assertEquals("Java", greeting.getCourseExpectation());
        assertEquals("MyGreeting{firstName='Kseniya', secondName='Mihailovna', lastName='Golova', bitbucketUrl=https://KGolova@bitbucket.org}", greeting.getEducationInfo());
        assertFalse(hobbies.isEmpty());
        assertEquals(3, (int) this.hobbies.size());

    }

}
